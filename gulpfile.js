const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const include = require('gulp-include');
const autoprefixer = require('gulp-autoprefixer');
var babel = require("gulp-babel");
const minify = require('gulp-minify');
const cleanCss = require('gulp-clean-css');
const htmlmin = require('gulp-htmlmin');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');
pug.compiler = require('gulp-pug');

function pugHtml() {
  return gulp.src('./src/pug/*.pug')
  .pipe(pug({
    doctype: 'html',
    pretty: true
  }))
}
function pugIndex() {
  return gulp.src('./src/index.pug')
  .pipe(pug({
    doctype: 'html',
    pretty: true
  }))
  .pipe(htmlmin({ collapseWhitespace: true }))
  .pipe(gulp.dest('./dist/'))
  .pipe(browserSync.stream());
}

function cssStyle() {
  return gulp.src('./src/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(concat('main.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('./dist/css'))
    .pipe(browserSync.stream());
}

function js() {
  return gulp.src('./src/js/index.js')
  	.pipe(include({
  		extensions: 'js',
  		includePaths: [
  			'.'
  		]
  	}))
    .pipe(gulp.dest('./dist/js'))
}

function watch() {
  browserSync.init({
    server: {
      baseDir: './dist/'
    }
  });
  gulp.watch('./src/**/*.js').on('change', browserSync.reload);
  gulp.watch('./src/**/*.pug', pugHtml);
  gulp.watch('./src/**/*.pug', pugIndex);
  gulp.watch('./src/**/*.scss', cssStyle);
  gulp.watch('./src/**/*.js', js);

}

exports.cssStyle = cssStyle;
exports.pugHtml = pugHtml;
exports.watch = watch;

exports.scripts = function (done) {
  gulp.src('source/js/entry.js')
    .pipe(include({
      extensions: 'js',
      hardFail: true,
      separateInputs: true,
      includePaths: [
        __dirname + '/bower_components',
        __dirname + '/src/js'
      ]
    }))
    .pipe(gulp.dest('dist/js'))
}

exports.build = function(done) {
  return gulp.src('./dist/js/*.js')
    .pipe(babel({
      presets: [
        ['@babel/env', {
          modules: false
        }]
      ]
    }))
    .pipe(minify({
      ext:{
          min:'.js'
      },
      noSource: true
    }))
    .pipe(gulp.dest('./dist/js'))
};
