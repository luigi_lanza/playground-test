const revealController = (bannerWrap) => {
  const bannerTimeline = gsap.timeline({paused: true});
  const timelineTime = .3;

  const bannerText = bannerWrap.querySelector('.banner-text')
  const bannerTextElements = bannerText.querySelectorAll('*');

  bannerTextElements.forEach((bannerTextElement, index) => {
    bannerTimeline.fromTo(bannerTextElement, 1, {opacity: 0}, {opacity: 1}, timelineTime*index);
  });

  bannerTimeline.play();
}

const bannerContentReveal = () => {
  const bannerWraps = document.querySelectorAll('.banner-wrap');
  const bannerScrollController = new ScrollMagic.Controller();

  bannerWraps.forEach(bannerWrap => {
    sectionScrollScene = new ScrollMagic.Scene({
      triggerElement: bannerWrap,
      triggerHook: 0.6
    })
    .on('enter', () => {
      if (!bannerWrap.classList.contains('revealed')) {
        revealController(bannerWrap);
        bannerWrap.classList.add('revealed')
      }
    })
    .addTo(bannerScrollController);

  });
}

window.addEventListener('load', () => {
  bannerContentReveal();
})
