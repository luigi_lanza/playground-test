const contactForm = document.querySelector('#contact_form');
let submitCheck = true;

const errorCode = (input, isEmail) => {
  const errorText = document.createElement('p');
  errorText.classList.add('error-text');
  if (isEmail) {
    errorText.innerHTML = "L'email deve essere di un formato corretto."
  }else{
    errorText.innerHTML = "Questo campo non può essere vuoto."
  }
  input.parentNode.append(errorText);
  input.classList.add('error');

  submitCheck = false;
}

const removeErrorCode = (input) => {
  const inputParent = input.parentNode;
  const erroText = inputParent.querySelector('.error-text');
  erroText.remove();
  input.classList.remove('error');
}

const formValidation = () => {
  const formInputs = contactForm.querySelectorAll('input');
  const formTextAreas = contactForm.querySelectorAll('textarea');

  formInputs.forEach(formInput => {
    if (formInput.hasAttribute('required')) {
      if (formInput.getAttribute('name') == 'email') {
        const mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (formInput.value == '' || formInput.value.match(mailformat) == null) {
          if (!formInput.classList.contains('error')) {
            errorCode(formInput, true);
          }
        }else{
          if (formInput.classList.contains('error')) {
            removeErrorCode(formInput);
          }
        }
      }else{
        if (formInput.value == '' && !formInput.classList.contains('error')) {
          errorCode(formInput, false);
        }else{
          if (formInput.classList.contains('error')) {
            removeErrorCode(formInput);
          }
        }
      }
    }
  });
  formTextAreas.forEach(formTextArea => {
    if (formTextArea.hasAttribute('required')) {
      if (formTextArea.value == '' && !formTextArea.classList.contains('error')) {
        errorCode(formTextArea, false);
      }else{
        if (formTextArea.classList.contains('error')) {
          removeErrorCode(formTextArea);
        }
      }
    }
  });
}

const buttonSubmit = () => {
  const formButton = contactForm.querySelector('.button-field');

  formButton.addEventListener('click', (e) => {
    formValidation();
  })

  contactForm.addEventListener('submit', (e) => {
    if (!submitCheck) {
      e.preventDefault();
    }
    submitCheck = true;
  });
}

window.addEventListener('load', () => {
  buttonSubmit();
})
