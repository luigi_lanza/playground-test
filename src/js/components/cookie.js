const cookieInit = () => {
  const cookieBar = document.querySelector('.section-cookie');
  if (cookieBar) {
    const cookieBarCta = cookieBar.querySelector('.cookie-button a');
    cookieBarCta.addEventListener('click', (e) => {
      e.preventDefault();
      cookieBar.remove();
    })
  }
}

window.addEventListener('load', () => {
  cookieInit();
})
