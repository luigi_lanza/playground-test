const footerNav = document.querySelector('.footer-nav');
const footerLinks = footerNav.querySelectorAll('.footer-link');

const footerScrollTo = (footerLink) => {
  const footerTarget = footerLink.getAttribute('href');
  const footerNav = document.querySelector('.navigation-bar');
  const footerNavHeight = footerNav.offsetHeight;
	gsap.to(window, .4, {
		scrollTo: {
			y: footerTarget,
			offsetY: footerNavHeight,
			autoKill: false
		},
		ease: Sine.easeInOut
	});
}

const footerScrollLink = () =>{
  if (document.querySelector('.section-footer')) {

    footerLinks.forEach(footerLink => {
      const footerTarget = footerLink.querySelector('a');
      footerTarget.addEventListener('click', (e) => {
        e.preventDefault();
        footerScrollTo(footerTarget);
      })
    });

  }
}

window.addEventListener('load', () => {
  footerScrollLink()
})
