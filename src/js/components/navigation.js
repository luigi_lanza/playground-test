const navigationBar = document.querySelector('.navigation-bar');
const navigationMenu = navigationBar.querySelector('.navigation-menu');
const menuLinks = navigationMenu.querySelectorAll('a');

const removeActiveLink = () => {
  if (navigationMenu.querySelector('.active')) {
    navigationMenu.querySelector('.active').classList.remove('active');
  }
}
const setActiveLink = (menuLink) =>{
  removeActiveLink();
  menuLink.classList.add('active');
}

const menuOnScroll = () => {
  const sections = document.querySelectorAll('section');

  let sectionScrollController = new ScrollMagic.Controller();
  sections.forEach(section => {
    let trigger;
    if (section.classList.contains('section-banner')) {
      trigger = section.querySelector('.banner-wrap');
    }else{
      trigger = section;
    }
    if (trigger.hasAttribute('id')) {
      const sectionId = trigger.getAttribute('id');
      const sectionHeight = trigger.offsetHeight;
      menuLinks.forEach(menuLink => {
        if (menuLink.getAttribute('href') == `#${sectionId}`) {
          sectionScrollScene = new ScrollMagic.Scene({
            triggerElement: trigger,
            triggerHook: 0.25,
            duration: sectionHeight
          })
          .setClassToggle(menuLink, 'active')
          .addTo(sectionScrollController);
        }
      });
    }
  });
}

const menuScrollTo = (menuLink) => {
  const menuTarget = menuLink.getAttribute('href');
  const navigationBar = document.querySelector('.navigation-bar');
  const navigationBarHeight = navigationBar.offsetHeight;

	gsap.to(window, .4, {
		scrollTo: {
			y: menuTarget,
			offsetY: navigationBarHeight,
			autoKill: false
		},
		ease: Sine.easeInOut
	});
}

const navigationActive = () =>{
  if (document.querySelector('.navigation-menu')) {

    menuLinks.forEach(menuLink => {
      menuLink.addEventListener('click', (e) => {
        e.preventDefault();
        if (!menuLink.classList.contains('active')) {
          setActiveLink(menuLink);
          menuScrollTo(menuLink);
        }
      })
    });

  }
}

const navigationBg = () => {
  if (window.pageYOffset != 0 || document.documentElement.scrollTop != 0 || document.body.scrollTop != 0) {
    if (!document.querySelector('.navigation-bar').classList.contains('black-nav')) {
      navigationBar.classList.add('black-nav');
    }
  }else{
    navigationBar.classList.remove('black-nav');
  }
}




window.addEventListener('load', () => {
  navigationActive();
  menuOnScroll();
})
window.addEventListener('scroll', () => {
  navigationBg();
})
