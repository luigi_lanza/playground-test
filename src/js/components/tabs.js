const tabs = document.querySelectorAll('.tab');
const tabDescription = document.querySelector('.tab-description');
let fakeTiming;
if (getUrlVar('tabs') == 'delay') {
  fakeTiming = 3000;
}else{
  fakeTiming = 0;
}

const tabBuild = (index, jsonArr, buildTitle) => {
  var i;
  if (buildTitle) {
    const jsonTitle = jsonArr.item.title;
    const activeTab = document.querySelector(`.tab[data-tab='${index}'] `);
    activeTab.querySelector('p').innerHTML = jsonTitle + ' <i class="fas fa-angle-up"></i>';
  }else{
    const jsonContent = jsonArr.item.content;
    let totalContent = '';
    jsonContent.forEach(content => {
      totalContent += content;
    });
    tabDescription.querySelector('p').innerHTML = totalContent;
  }
}

const tabsInit = (index, buildTitle) => {
  if (typeof index == 'undefined') {
    index = 1;
  }
  var xmlhttp = new XMLHttpRequest();
  var url = `../assets/ajax/tab${index}.json`;

  xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var jsonArr = JSON.parse(this.responseText);
      if (buildTitle) {
        tabBuild(index, jsonArr, buildTitle);
      }else{
        tabBuild(index, jsonArr);
      }
      setTimeout(() => {
        tabDescription.classList.add('loaded');
      }, fakeTiming);
    }
  };
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

const tabsTitle = () => {
  const buildTitle = true;
  tabs.forEach(tab => {
    const tabIndex = tab.getAttribute('data-tab');
    if (tabIndex == 1) {
      tab.classList.add('active');
    }
    tabsInit(tabIndex, buildTitle)
  });
}

const tabsClick = () => {
  tabs.forEach(tab => {
    tab.addEventListener('click', (e) => {
      const tabIndex = tab.getAttribute('data-tab');
      if (!tab.classList.contains('active')) {
        if (document.querySelector('.tab.active')) {
          document.querySelector('.tab.active').classList.remove('active');
        }
        tab.classList.add('active');
        tabDescription.classList.remove('loaded');
        tabsInit(tabIndex);
      }
    })
  });

}

window.addEventListener('load', () => {
  tabsTitle();
  tabsInit();
  tabsClick();
})
