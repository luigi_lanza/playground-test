const visualSliderInit = () => {
  if (document.querySelector('.visual-swiper')) {
    const visualSwiper = new Swiper('.visual-swiper', {
      simulateTouch: false,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    })
  }
}





window.addEventListener('load', () => {
  visualSliderInit();
})
