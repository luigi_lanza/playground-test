let vw = window.innerWidth;
const dynamicHeight = () =>{
  let vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', `${vh}px`);
}
const ieFix = () => {
  // IE Fix per il .foreach
  if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
  }
}

window.addEventListener('resize', () => {
  if (vw != window.innerWidth) {
    dynamicHeight();
    vw = window.innerWidth
  }
})

window.addEventListener('load', () => {
  dynamicHeight();
  ieFix();
})
