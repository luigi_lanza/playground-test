const getUrlVars = () => {
	var vars = [], hash;
  const windowLocation = window.location.href;
	var splitCode = windowLocation.indexOf("&amp;") > 0 ? "&amp;" : "&";
	var hashes = windowLocation
		.slice(windowLocation.indexOf("?") + 1)
		.split(splitCode);

	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split("=");
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
};

const getUrlVar = (name) => {
	return getUrlVars()[name];
};
