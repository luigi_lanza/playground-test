//SWIPER
//=include node_modules/swiper/swiper-bundle.js

//GSAP
//=include node_modules/gsap/dist/gsap.min.js
//=include node_modules/gsap/dist/ScrollToPlugin.min.js

// scrollmagic
//=include node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js


//=include ./config.js
//=include ./functions.js

//COMPONENTS
//=include ./components/navigation.js
//=include ./components/visual.js
//=include ./components/cards.js
//=include ./components/banner.js
//=include ./components/tabs.js
//=include ./components/contacts.js
//=include ./components/footer.js
//=include ./components/cookie.js
